import {default as filterReducer} from '../src/layout/actionBar/components/filter/Filter.reducers.js';

const LAYOUT_ACTIONBAR_FILTER_CHANGE = 'LAYOUT_ACTIONBAR_FILTER_CHANGE';

describe('filter reducer', () => {
    it('should return the initial state', () => {
        expect(filterReducer(undefined, {})).toEqual({filterCriterion: ""})
    });

    it('should return new state', () => {
        expect(filterReducer({}, {
            type: LAYOUT_ACTIONBAR_FILTER_CHANGE,
            data: {filterCriterion: 'Maths'}
        })).toEqual({filterCriterion: "Maths"})
    });

    it('should return changed state', () => {
        expect(filterReducer({filterCriterion: 'Maths'}, {
            type: LAYOUT_ACTIONBAR_FILTER_CHANGE,
            data: {filterCriterion: 'Geography'}
        })).toEqual({filterCriterion: "Geography"})
    });
});