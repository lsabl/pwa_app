import {default as titleReducer} from '../src/layout/actionBar/components/title/Title.reducers.js';

const LAYOUT_ACTIONBAR_TITLE_CHANGE = 'LAYOUT_ACTIONBAR_TITLE_CHANGE';

describe('title reducer', () => {
    it('should return the initial state', () => {
        expect(titleReducer(undefined, {})).toEqual({title: "Mindfight"})
    });

    it('should return new state', () => {
        expect(titleReducer({}, {
            type: LAYOUT_ACTIONBAR_TITLE_CHANGE,
            data: {title: 'Question 1'}
        })).toEqual({title: "Question 1"})
    });

    it('should return changed state', () => {
        expect(titleReducer({title: 'Question 1'}, {
            type: LAYOUT_ACTIONBAR_TITLE_CHANGE,
            data: {title: 'Question 2'}
        })).toEqual({title: "Question 2"})
    });
});