import {default as homeReducers} from '../src/home/Home.reducers.js';

const HOME_MINDFIGHTS_ADD = 'HOME_MINDFIGHTS_ADD';

const mockMindfight = {
    "name" : "Pirmas kartas kaime",
    "topic" : "Gamta",
    "questions": [
        {
            "question" : "Kokios spalvos būna jūrų kiaulytės?",
            "allAnswers" : [
                "Geltonos",
                "Baltos",
                "Oranžinės",
                "Raudonos"
            ],
            "validAnswers" : [
                "Baltos",
                "Oranžinės"
            ],
            "time" : 10
        },
        {
            "question" : "Kaip dažnai reikia vesti šunį į lauką?",
            "allAnswers" : [
                "Kas valandą",
                "Kas 2-3 valandas",
                "Kartą į dieną",
                "Jei gerai dresiruotas iš vis nereikia vesti"
            ],
            "validAnswers" : [
                "Kas 2-3 valandas",
                "Jei gerai dresiruotas iš vis nereikia vesti"
            ],
            "time" : 10
        },
        {
            "question" : "Kur maudosi šinšilos?",
            "allAnswers" : [
                "Vandenyje",
                "Smėlyje",
                "Purve"
            ],
            "validAnswers" : [
                "Smėlyje"
            ],
            "time" : 10
        },
        {
            "question" : "Kurie yra naminiai gyvūnai?",
            "allAnswers" : [
                "Šuo",
                "Katė",
                "Vilkas"
            ],
            "validAnswers" : [
                "Šuo",
                "Katė"
            ],
            "time" : 10
        }
    ]
};

describe('home reducer', () => {
    it('should return the initial state', () => {
        expect(homeReducers(undefined, {})).toEqual({mindfights: []})
    });

    it('should return new state', () => {
        expect(homeReducers({}, {
            type: HOME_MINDFIGHTS_ADD,
            data: {mindfights: [mockMindfight, mockMindfight]}
        })).toEqual({mindfights: [mockMindfight, mockMindfight]})
    });

    it('should return changed state', () => {
        expect(homeReducers({mindfights: [mockMindfight]}, {
            type: HOME_MINDFIGHTS_ADD,
            data: {mindfights: [mockMindfight, mockMindfight]}
        })).toEqual({mindfights: [mockMindfight, mockMindfight]})
    });
});