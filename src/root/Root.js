import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';
import { App } from '../app';
import { Home } from '../home';
import { List } from '../list';
import { Question } from '../question';
import { Results } from '../results';

const Root = ({ store }) => (
    <Provider store={store}>
        <Router>
            <div>
                <Route path='/' component={App} />
                <Route exact path='/' component={Home} />
                <Route path='/list' component={List} />
                <Route path='/mindfight/:id/question/:nr' component={Question} />
                <Route path='/mindfight/:id/results' component={Results} />
                <Redirect from='*' to='/' />
            </div>
        </Router>
    </Provider>
);

export default Root;