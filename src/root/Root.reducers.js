import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { homeReducers } from '../home';
import { questionReducers } from '../question';
import { layoutReducer } from '../layout';

const rootReducer = combineReducers({
    form: formReducer,
    home: homeReducers,
    layout: layoutReducer,
    gameplay: questionReducers
});

export default rootReducer;