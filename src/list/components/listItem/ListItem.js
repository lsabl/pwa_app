import React, {Component} from 'react';
import {Link} from 'react-router-dom';

class ListItem extends Component {
    render() {
        const title = this.props.title;
        const linkTo = this.props.linkTo;
        return (
            <div className="ui raised very padded text container segment block">
                <Link to={linkTo}>
                    <h2 className="ui header">{title}</h2>
                </Link>
            </div>
        );
    }
}

export default ListItem;