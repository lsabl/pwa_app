import {connect} from 'react-redux';
import React, {Component} from 'react';
import {ListItem} from './components';

class List extends Component {
    render() {
        let mindfightList = [...this.props.mindfights];
        if (this.props.filterCriterion) {
            mindfightList = mindfightList.filter(mindfight => (mindfight.topic === this.props.filterCriterion ));
        }

        const filteredMindfightList = mindfightList.map(mindfight => (
            <ListItem
                title={mindfight.name}
                key={mindfight.uid}
                linkTo={`/mindfight/${mindfight.uid}/question/0`}/>
        ));

        return (
            <div>
                {filteredMindfightList}
            </div>
        );
    }
}

const mapStateToProps = state => ({...state.home, filterCriterion: state.layout.actionBar.filter.filterCriterion});

export default connect(mapStateToProps, {})(List);