const LAYOUT_ACTIONBAR_TITLE_CHANGE = 'LAYOUT_ACTIONBAR_TITLE_CHANGE';

const changeTitle = title => ({
    type: LAYOUT_ACTIONBAR_TITLE_CHANGE,
    data: { title }
});

const layoutActions = {
    changeTitle
};

export default layoutActions;