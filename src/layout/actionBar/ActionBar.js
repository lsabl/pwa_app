import React, { Component } from 'react';
import { BackButton, Filter, Title } from './components';

class ActionBar extends Component {
    render() {
        const url = window.location.pathname;

        let filter;
        let content = <Title/>;
        if (url === '/list') {
            content = <BackButton/>;
            filter = <Filter id="1"/>;
        }

        return (
            <div>
                <header className='action-bar-header'>
                    {content}
                    {filter}                    
                </header>
            </div>
        );
    }
}

export default ActionBar;