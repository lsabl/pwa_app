import { combineReducers } from 'redux';
import { filterReducer } from './components'
import { titleReducer } from './components';

export default combineReducers({
    filter: filterReducer,
    title: titleReducer
});