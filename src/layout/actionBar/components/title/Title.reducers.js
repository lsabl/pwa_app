const LAYOUT_ACTIONBAR_TITLE_CHANGE = 'LAYOUT_ACTIONBAR_TITLE_CHANGE';
const defaultState = { title: "Mindfight" };

const titleReducer = (state = defaultState, action) => {
    switch (action.type) {
        case LAYOUT_ACTIONBAR_TITLE_CHANGE: {
            return { ...state, title: action.data.title };
        }
        default: {
            return state;
        }
    }
};

export default titleReducer;