import React, { Component } from 'react';
import { connect } from 'react-redux';

class Title extends Component {
    render() {
        return (
            <h1 className='header__title'>{this.props.title}</h1>
        );
    }
}

const mapStateToProps = state => ({ title: state.layout.actionBar.title.title });

export default connect(mapStateToProps, {})(Title);