const LAYOUT_ACTIONBAR_FILTER_CHANGE = 'LAYOUT_ACTIONBAR_FILTER_CHANGE';

export const changeFilter = filterCriterion => ({
    type: LAYOUT_ACTIONBAR_FILTER_CHANGE,
    data: { filterCriterion }
});