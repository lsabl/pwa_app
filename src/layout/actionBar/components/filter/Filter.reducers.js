const LAYOUT_ACTIONBAR_FILTER_CHANGE = 'LAYOUT_ACTIONBAR_FILTER_CHANGE';
const defaultState = { filterCriterion: "" };

const filterReducer = (state = defaultState, action) => {
    switch (action.type) {
        case LAYOUT_ACTIONBAR_FILTER_CHANGE: {
            return { ...state, filterCriterion: action.data.filterCriterion };
        }
        default: {
            return state;
        }
    }
};

export default filterReducer;