import React, { Component } from 'react';
import { connect } from 'react-redux';
import { changeFilter } from './Filter.actions';

class Filter extends Component {
  static propTypes = {
    id: React.PropTypes.string.isRequired,
  };

  constructor(props, context) {
    super(props, context);

    this.state = {
      dropdownIsActive: false,
      dropdownIsVisible: false,
    };

    this._hideDropdown = this._hideDropdown.bind(this);
    this._toggleDropdown = this._toggleDropdown.bind(this);
    this._handleFocus = this._handleFocus.bind(this);
    this._handleBlur = this._handleBlur.bind(this);
  }


  componentDidMount() {
    window.addEventListener('click', this._hideDropdown, false);
  }

  componentWillUnmount() {
    window.removeEventListener('click', this._hideDropdown, false);
  }

  _stopPropagation(e) {
    e.stopPropagation();
  }

  _toggleDropdown() {
    const { dropdownIsVisible } = this.state;
    this.setState({ dropdownIsVisible: !dropdownIsVisible });
  }

  _hideDropdown() {
    const { dropdownIsActive } = this.state;

    if (!dropdownIsActive) {
      this.setState({ dropdownIsVisible: false });
    }
  }

  _handleFocus() {
    this.setState({ dropdownIsActive: true });
  }

  _handleBlur() {
    this.setState({
      dropdownIsVisible: false,
      dropdownIsActive: false,
    });
  }

  _handleItemClick(topic) {
    this.props.actions.filterChange(topic);
  }

  _renderDropdown() {
    const dropdownId = this.props.id;
    const { dropdownIsVisible } = this.state;
    const topics = [...this.props.mindfights].map(mindfight => (
      <div key={mindfight.topic} className="item" onClick={() => this._handleItemClick(mindfight.topic)}>
        {mindfight.topic}
      </div>
    ));
    topics.unshift(<div key="All" className="item" onClick={() => this._handleItemClick("")}>All</div>);
    return (
      <div
        className="wrapper"
        tabIndex={dropdownId}
        onFocus={this._handleFocus}
        onBlur={this._handleBlur}
        onClick={this._toggleDropdown}
      >
        <span className="toggler">
          Filter
        </span>
        {
          dropdownIsVisible &&
          <div className="content">
            {topics}
          </div >
        }
      </div >
    );
  }
  render() {
    return (
      <div className="dropdown">
        {this._renderDropdown()}
      </div>
    );
  }
}

const mapStateToProps = state => ({ ...state.home });
const mapDispatchToProps = dispatch => (
  {
    actions: {
      filterChange: (topic) => dispatch(changeFilter(topic))
    }
  });


export default connect(mapStateToProps, mapDispatchToProps)(Filter);

