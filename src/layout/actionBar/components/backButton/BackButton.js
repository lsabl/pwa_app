import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class BackButton extends Component {
    render() {
        return (
            <Link to="/">
                <img src="button.png" alt="Back button" className='back-button'/>
            </Link>
        );
    }
}

export default BackButton;