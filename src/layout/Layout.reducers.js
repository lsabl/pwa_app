import { combineReducers } from 'redux';
import { actionBarReducer } from './actionBar'

export default combineReducers({
    actionBar: actionBarReducer
});