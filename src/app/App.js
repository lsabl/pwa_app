import React, { Component } from 'react';
import { ActionBar } from '../layout';
class App extends Component {
  render() {
    return (
      <div className='App'>
        <ActionBar/>
      </div>
    );
  }
}

export default App;