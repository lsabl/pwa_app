import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as resultsActions from './Results.actions';
import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {ControlButton, Result} from './components';

class Results extends Component {
    componentWillMount() { this.props.actions.changeTitle(`Results`); }
    render() {        
        const answeredCorrectly = this.props.answeredCorrectly;
        const totalCorrectAnswers = this.props.totalCorrectAnswers;
        const score = Math.max(answeredCorrectly / totalCorrectAnswers * 100, 0).toFixed(2);

        return (
            <div>
                <Link to={`/mindfight/${this.props.match.params.id}/question/0`}>
                    <Result score={`${score}`}/>
                    <ControlButton title={'Play again'}/>
                </Link>
                <Link to={`/`}>
                    <ControlButton title={'Back to main menu'}/>
                </Link>
                <Link to={`/list`}>
                    <ControlButton title={'MindFight list'}/>
                </Link>
            </div>
        );
    }

    componentWillUnmount() {
        this.props.actions.clearQuestionState();
    }
}

const mapStateToProps = state => (
    {
        answeredCorrectly: state.gameplay.questions.answeredCorrectly,
        totalCorrectAnswers: state.gameplay.questions.totalCorrectAnswers
    });

const mapDispatchToProps = dispatch => ({actions: bindActionCreators(resultsActions, dispatch)});

export default connect(mapStateToProps, mapDispatchToProps)(Results);