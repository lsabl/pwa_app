import {layoutActions} from '../layout';
import {questionActions} from '../question';

export const changeTitle = title => {
    return dispatch => {
        dispatch(layoutActions.changeTitle(title));
    };
};

export const clearQuestionState = () => {
    return dispatch => {
        dispatch(questionActions.clearQuestionState());
    };
};
