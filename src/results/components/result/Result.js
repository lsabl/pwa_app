import React, { Component } from 'react';

class Result extends Component {
  render() {
    return (
        <h2 className="ui header score">Your's score is {this.props.score} %</h2>
    );
  }
}

export default Result;