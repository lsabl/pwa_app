import React, { Component } from 'react';

class ControlButton extends Component {
  render() {
      const title = this.props.title;
    return (
      <div className="ui raised very padded text container segment">
        <h2 className="ui header">{title}</h2>
    </div>
    );
  }
}

export default ControlButton;