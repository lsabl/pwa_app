import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { default as questionActions } from './Question.actions';
import { QuestionField, Answers, Timer } from './components';

class Question extends Component {
    render() {
        this.props.actions.changeTitle(`Question ${+this.props.match.params.nr + 1}`);

        const onSubmitClick = (validAnswers) => {
            this.props.actions.submitClick(validAnswers);
        }

        const onNextClick = () => {
            this.props.actions.nextClick();
        }
        const setNextLinkUrl = () => {
            if (+this.props.match.params.nr + 1 === this.props.mindfight.questions.length) {
                return `/mindfight/${mindfight.uid}/results`;
            } else {
                return `/mindfight/${mindfight.uid}/question/${+this.props.match.params.nr + 1}`;
            }
        };

        const mindfight = this.props.mindfight;
        const currentQuestion = mindfight.questions[+this.props.match.params.nr];
        const nextLinkUrl = setNextLinkUrl();
        const phase = this.props.gameplay.questions.phase;
        if (phase === "SUBMIT") {
            return (
                <div>
                    <QuestionField question={currentQuestion.question} />
                    <div className="answers-area">
                        <div className="answers">
                            <Answers allAnswers={currentQuestion.allAnswers} validAnswers={currentQuestion.validAnswers} phase={phase} />
                        </div>
                        <div className="question-actions">
                            <Timer secondsRemaining={currentQuestion.time} handleTermination={() => onSubmitClick(currentQuestion.validAnswers)} />
                            <button className="submit-btn" onClick={() => onSubmitClick(currentQuestion.validAnswers)}>SUBMIT</button>
                        </div>
                    </div>
                </div>
            );
        }
        else if (phase === "NEXT") {
            return (
                <div>
                    <QuestionField question={currentQuestion.question} />
                    <div className="answers-area">
                        <div className="answers">
                            <Answers
                                allAnswers={currentQuestion.allAnswers}
                                validAnswers={currentQuestion.validAnswers} phase={phase} />
                        </div>
                        <div className="question-actions">
                            <Link className="question-btn" to={nextLinkUrl} key={mindfight.uid}>
                                <button onClick={onNextClick}>NEXT</button>
                            </Link>
                        </div>
                    </div>
                </div>
            );
        }
    }
}

const mapStateToProps = (state, ownProps) => (
    {
        mindfight: [...state.home.mindfights].find(mindfight => (mindfight.uid === ownProps.match.params.id)),
        gameplay: state.gameplay
    });
const mapDispatchToProps = dispatch => ({ actions: bindActionCreators(questionActions, dispatch) });

export default connect(mapStateToProps, mapDispatchToProps)(Question);