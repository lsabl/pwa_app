import { layoutActions } from '../layout';

const QUESTION_SUBMIT_CLICK = 'QUESTION_SUBMIT_CLICK';
const QUESTION_NEXT_CLICK = 'QUESTION_NEXT_CLICK';
const QUESTION_ADD_SCORE = 'QUESTION_ADD_SCORE';
const QUESTION_CLEAR_STATE = 'QUESTION_CLEAR_STATE';
const CLEAR_CHOSEN_ANSWERS = 'CLEAR_CHOSEN_ANSWERS';

const clearQuestionState = () => ({
    type: QUESTION_CLEAR_STATE,
    data: {}
});

const submitClick = validAnswers => {
    return (dispatch, getState) => {
        dispatch({
            type: QUESTION_SUBMIT_CLICK,
            data: { phase: 'NEXT' }
        });
        const chosenAnswers = getState().gameplay.answers.chosenAnswers;
        const answeredCorrectly = chosenAnswers.reduce((acc, val) =>
            {
                return acc + (validAnswers.includes(val) ? +process.env.REACT_APP_CORRECT_ANSWER_SCORE : +process.env.REACT_APP_WRONG_ANSWER_SCORE)
            }
            , 0);
        dispatch({
            type: QUESTION_ADD_SCORE,
            data: { answeredCorrectly, totalCorrectAnswers: validAnswers.length }
        });
        dispatch({ type: CLEAR_CHOSEN_ANSWERS });
    };

};

const nextClick = () => {

    return dispatch => {
        dispatch({
            type: QUESTION_NEXT_CLICK,
            data: {
                phase: 'SUBMIT'
            }
        });
    };
};

const changeTitle = title => {
    return dispatch => {
        dispatch(layoutActions.changeTitle(title));
    };
};

const questionActions = {
    clearQuestionState,
    submitClick,
    nextClick,
    changeTitle
};

export default questionActions;