import React, {Component} from 'react';

class Timer extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            secondsRemaining: props.secondsRemaining
        };
        this.tick = this.tick.bind(this);
        this.interval = setInterval(this.tick, 1000);
    }

    tick() {
        this.setState({secondsRemaining: this.state.secondsRemaining - 1});
        if (this.state.secondsRemaining <= 0) {
            clearInterval(this.interval);
            this.props.handleTermination();
        }
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        return (
            <div>Seconds Remaining: {this.state.secondsRemaining}</div>
        );
    }
}

export default Timer;
