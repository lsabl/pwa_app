const SELECT_ANSWER = 'SELECT_ANSWER';
const CLEAR_CHOSEN_ANSWERS = 'CLEAR_CHOSEN_ANSWERS';

const selectAnswer = title => ({
    type: SELECT_ANSWER,
    data: { title }
});

const clearAnswers = () => ({
    type: CLEAR_CHOSEN_ANSWERS
});

const select = title => {
    return dispatch => {
        dispatch(selectAnswer(title));               
    };
};

const clearChosenAnswers = () => {
    return dispatch => {
        dispatch(clearAnswers());               
    };
};

const answerActions = {
    select,
    clearChosenAnswers
};

export default answerActions;