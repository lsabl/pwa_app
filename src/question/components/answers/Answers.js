import React, { Component } from 'react';
import { default as answerActions } from './Answers.actions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class Answers extends Component {
    render() {
        const allAnswers = this.props.allAnswers;
        const validAnswers = this.props.validAnswers;
        const phase = this.props.phase;
        let answers = [];

        const onChbChange = (answer) => {
            this.props.actions.select(answer);
        };

        if (phase === "SUBMIT") {
            answers = [...allAnswers].map(answer => (
                <div className="answer-wrapper" key={answer}>
                    <div className="ui checkbox" key={answer}>
                        <input name="example" type="checkbox" onChange={() => onChbChange(answer)} key={answer}/>
                        <label>{answer}</label>
                    </div>
                </div>)
            );
        }
        else if (phase === "NEXT") {
            const isAnswerValid = (answer) => (validAnswers.find(validAnswer => (answer === validAnswer)));
            answers = [...allAnswers].map(answer => {
                return (
                    <div className="answer-wrapper" key={answer}>
                        <div className={`ui checkbox answer ${isAnswerValid(answer) ? 'valid' : 'invalid'}`} key={answer}>
                            <input name="example" type="checkbox" disabled="disabled" key={answer}/>
                            <label>{answer}</label>
                        </div>
                    </div>);
            });
        }
        return (
            <div>
                {answers}
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => ({ actions: bindActionCreators(answerActions, dispatch) });

export default connect(null, mapDispatchToProps)(Answers);