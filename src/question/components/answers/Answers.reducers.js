const SELECT_ANSWER = 'SELECT_ANSWER';
const CLEAR_CHOSEN_ANSWERS = 'CLEAR_CHOSEN_ANSWERS';

const defaultState = { chosenAnswers: [] };

const answerReducers = (state = defaultState, action) => {
    switch (action.type) {
        case SELECT_ANSWER: {
            let chosenAnswersClone;
            if (!state.chosenAnswers.includes(action.data.title)) {
                chosenAnswersClone = [...state.chosenAnswers.slice(), action.data.title];
            }
            else {
                chosenAnswersClone = [...state.chosenAnswers.slice()];
                let indexToRemove = chosenAnswersClone.indexOf(action.data.title);
                chosenAnswersClone.splice(indexToRemove, 1);

            }
            return { chosenAnswers: chosenAnswersClone };
        }
        case CLEAR_CHOSEN_ANSWERS: {
            return defaultState;
        }
        default: {
            return state;
        }
    }
};

export default answerReducers;