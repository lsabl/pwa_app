import React, {Component} from 'react';

class QuestionField extends Component {
    render() {
        const question = this.props.question;
        return (
            <div className="ui raised very padded text container segment block">
                <h2 className="ui header">{question}</h2>
            </div>
        );
    }
}

export default QuestionField;
