import { combineReducers } from 'redux';
import { AnswersReducers } from './components/answers'

const QUESTION_SUBMIT_CLICK = 'QUESTION_SUBMIT_CLICK';
const QUESTION_NEXT_CLICK = 'QUESTION_NEXT_CLICK';
const QUESTION_ADD_SCORE = 'QUESTION_ADD_SCORE';
const QUESTION_CLEAR_STATE = 'QUESTION_CLEAR_STATE';

const defaultState = { phase: "SUBMIT", answeredCorrectly: 0, totalCorrectAnswers: 0 };

const questionReducers = (state = defaultState, action) => {
    switch (action.type) {
        case QUESTION_SUBMIT_CLICK: {
            return { ...state, phase: action.data.phase }
        }
        case QUESTION_NEXT_CLICK: {
            return { ...state, phase: action.data.phase }
        }
        case QUESTION_ADD_SCORE: {
            return {
                ...state,
                answeredCorrectly: action.data.answeredCorrectly + state.answeredCorrectly,
                totalCorrectAnswers: action.data.totalCorrectAnswers + state.totalCorrectAnswers
            }
        }
        case QUESTION_CLEAR_STATE: {
            return defaultState
        }
        default: {
            return state;
        }
    }
};

export default combineReducers({
    answers: AnswersReducers,
    questions: questionReducers
});