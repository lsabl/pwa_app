import React, {Component} from 'react';
import {Link} from 'react-router-dom';

class Mode extends Component {
    render() {
        const top = {
            top: 14
        };
        return (
            <div style={top} className="ui raised very padded text container segment">
                <Link to="/list">
                    <h2 className="ui header">Training</h2>
                </Link>
            </div>
        );
    }
}

export default Mode;