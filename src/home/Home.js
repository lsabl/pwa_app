import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import React, { Component } from 'react';
import { GameModes } from './components';
import * as homeActions from './Home.actions';

class Home extends Component {
  componentWillMount() {
    this.props.actions.initDB();
    this.props.actions.changeTitle('Mindfight');
  }

  render() {
    return (
      <div>
        <GameModes /> 
      </div>
    );
  }
}

const mapStateToProps = state => ({ ...state });
const mapDispatchToProps = dispatch => ({ actions: bindActionCreators(homeActions, dispatch) });

export default connect(mapStateToProps, mapDispatchToProps)(Home);