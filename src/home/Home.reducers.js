const HOME_MINDFIGHTS_ADD = 'HOME_MINDFIGHTS_ADD';
const defaultState = { mindfights: [] };

const homeReducers = (state = defaultState, action) => {
    switch (action.type) {
        case HOME_MINDFIGHTS_ADD: {
            return { ...state, mindfights: action.data.mindfights };
        }
        default: {
            return state;
        }
    }
};

export default homeReducers;