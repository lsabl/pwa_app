import pouchDB from 'pouchdb';
import { layoutActions } from '../layout';

const HOME_MINDFIGHTS_ADD = 'HOME_MINDFIGHTS_ADD';

const addMindfights = mindfights => ({
    type: HOME_MINDFIGHTS_ADD,
    data: { mindfights }
});

export const changeTitle = title => {
    return dispatch => {
        dispatch(layoutActions.changeTitle(title));
    };
};

export const initDB = () => {
    return dispatch => {
        const db = new pouchDB(process.env.REACT_APP_POUCH_DB_MINDFIGHTS, { revs_limit: 100 });

        const fetchMindfightsAndUpdate = () => {
            const replaceMindfights = mindfights => {
                db.destroy((err, response) => {
                    if (err) {
                        return console.log(err);
                    } else {
                        const db = new pouchDB(process.env.REACT_APP_POUCH_DB_MINDFIGHTS, { revs_limit: 100 });
                        db.bulkDocs(mindfights, err => {
                            if (err) {
                                return console.log(err);
                            } else {
                                dispatch(addMindfights(mindfights));
                            }
                        });
                    }
                });
            };

            fetch(`${process.env.REACT_APP_SERVICE_HOST}/mindfights`, {
                mode: 'cors',
                method: 'GET'
            }).then(res => {
                if (res.ok) {
                    res.json().then(results => {
                        replaceMindfights(results);
                    });
                } else {
                    console.error(res.statusText);
                }
            }).catch(err => {
                console.error(err);
            });
        };

        const restoreMindfights = () => {
            db.allDocs({ include_docs: true }, (err, results) => {
                if (err) {
                    console.error('Unable to retrieve mindfights from indexedDB.');
                    console.error(err);
                } else {
                    const mindfights = [];
                    results.rows.forEach((row) => mindfights.push(row.doc));
                    dispatch(addMindfights(mindfights));
                }
            });
        };

        restoreMindfights();

        fetchMindfightsAndUpdate();
    };
};