Application starts online and saves the questions/answers for playing offline. You can either access it by entering the URL, or from an icon that appeared on your device's home screen after the first time. Both of these options don't require an internet connection.

First you select what **mode** the game will be in. Now only show "Training".

After going to "Training", you then select **topic**, such as "Math", "History", etc. 

From there the quiz starts. You are shown:

* A question;
* A count of how many question are left;
* A countdown timer with X seconds;
* Buttons A/B/C/.. or YES/NO which you can select as your answer. If the selected answer is correct, the selected button lights up green, the other buttons red.

When you finish all the questions you are shown how many questions you have answered correctly.